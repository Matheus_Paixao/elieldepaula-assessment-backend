<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Classe model de categorias.
 *
 * @author Eliel de Paula <dev@elieldepaula.com.br>
 */
class Category extends Model
{
    
    /**
     * Relacionamento N:M entre categorias e protudos.
     * @return mixed
     */
    public function product()
    {
        return $this->belongsToMany('App\Models\Product', 'product_category');
    }
    
}
