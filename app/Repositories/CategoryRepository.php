<?php

namespace App\Repositories;
use App\Models\Category;
use Illuminate\Database\Eloquent\Collection;
use App\Interfaces\CrudInterface;

/**
 * Description of CategoryRepository
 *
 * @author elieldepaula
 */
class CategoryRepository implements CrudInterface
{
    
    protected $colection;
    
    function __construct()
    {
        $this->colection = new Category();
    }
    
    /**
     * Retorna um registro.
     * @param int $id
     */
    public function find(int $id)
    {
        return $this->colection->find($id);
    }
    
    /**
     * Retorna o primeiro registro usando Where.
     * 
     * @param string $var
     * @return object
     */
    public function whereOne($var)
    {
        return $this->colection->where('name', $var)->first();
    }
    
    /**
     * Lista todos os registros.
     */
    public function list() : Collection
    {
        return $this->colection->all();
    }
    
    /*
     * Cria um novo registro.
     */
    public function new(object $object)
    {
        $this->colection->name = $object->name;
        $this->colection->code = $object->code;
        return $this->colection->save();
    }
    
    /**
     * Cria um novo registro para o importador de arquivos. A diferença entre o
     * metodo new() e que este retorna o objeto recem criado.
     * 
     * @param object $object
     * @return object
     */
    public function newImport(object $object)
    {
        $this->colection->name = $object->name;
        $this->colection->code = $object->code;
        $this->colection->save();
        return $this->colection;
    }
    
    /**
     * Edita um registro.
     * 
     * @param Int $id ID do registro que sera editado.
     */
    public function edit(object $object, Int $id = null)
    {
        $category = $this->colection->find($id);
        $category->name = $object->name;
        $category->code = $object->code;
        return $category->save(); 
    }
    
    /**
     * Exclui um registro.
     * @param Int $id ID do registro que sera editado.
     */
    public function del(Int $id = null)
    {
        return $this->colection->destroy($id);
    }

}
