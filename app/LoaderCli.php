<?php

/**
 * Este arquivo faz o carregamento do projeto, ele sobe o autoload do Composer,
 * faz a instancia do roteador como aplicaçao e mantem as rotas do projeto.
 * 
 * Uma boa pratica seria separar as rotas em um arquivo separado caso o projeto
 * tome maiores proporçoes.
 * 
 * @author Eliel de Paula <dev@elieldepaula.com.br>
 */

require BASE_PATH . 'vendor/autoload.php';

require BASE_PATH . 'app/Database.php';

use App\Controllers\ImporterController;

