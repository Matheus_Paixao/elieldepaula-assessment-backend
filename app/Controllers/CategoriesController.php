<?php

namespace App\Controllers;
use App\Repositories\CategoryRepository as Category;
use App\Helpers\InputHelper as Input;
use App\Helpers\SessionHelper;

/**
 * Controller com as funcionalidades para gerenciar as categorias de produtos.
 *
 * @author Eliel de Paula <dev@elieldepaula.com.br>
 */
class CategoriesController extends BaseController
{
    
    public function all() 
    {
        
        $category        = new Category();
        $categoryList    = $category->list();
        
        $this->setVar('pageTitle', 'Categories');
        $this->setVar('categoryList', $categoryList);
        $this->setVar('msg', SessionHelper::getTemp('msg'));
        $this->render('categories.html');
        
    }
    
    public function create() 
    {
        $this->setVar('formAction', 'categories/create');
        $this->setVar('pageTitle', 'New Category');
        $this->render('formCategory.html');
    }
    
    public function edit($id) 
    {
        $category = new Category();
        $catEdit  = $category->find($id);
        
        $this->setVar('formAction', 'categories/'.$id.'/edit');
        $this->setVar('pageTitle', 'Edit Category');
        $this->setVar('category', $catEdit);
        $this->render('formCategory.html');
    }
    
    public function save($id = null) 
    {
        $category = new Category();
        $object = (object) [
            'name' => Input::Post('name'),
            'code' => Input::Post('code')
        ];

        if ($id===null) {
            
            if ($category->new($object)){
                $this->log('Category created successfully!', ['username' => 'Undefined', 'categoryName' => $object->name]);
                SessionHelper::set('msg', 'Category created successfully!');
                header('Location: /categories');
            } else {
                $this->log('Error creating category.', $object);
                SessionHelper::set('msg', 'Error creating category.', ['username' => 'Undefined', 'categoryName' => $object->name]);
                header('Location: /categories');
            }
            
        } else {
            
            if ($category->edit($object, $id)){
                $this->log('Category updated successfully!', ['username' => 'Undefined', 'categoryName' => $object->name]);
                SessionHelper::set('msg', 'Category updated successfully!');
                header('Location: /categories');
            } else {
                $this->log('Error updating category.', ['username' => 'Undefined', 'categoryName' => $object->name]);
                SessionHelper::set('msg', 'Error updating category.');
                header('Location: /categories');
            }
            
        }
    }
    
    public function delete($id) 
    {
        $category = new Category();
        if ($category->del($id)){
            $this->log('Category deleted successfully!', ['username' => 'Undefined', 'id' => $id]);
            SessionHelper::set('msg', 'Category deleted successfully!');
            header('Location: /categories');
        } else {
            $this->log('Error deleting category.', ['username' => 'Undefined', 'id' => $id]);
            SessionHelper::set('msg', 'Error deleting category.');
            header('Location: /categories');
        }
    }
}
