<?php

namespace App\Controllers;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;

/**
 * Esta classe encapsula recursos comuns aos controllers do tipo CLI.
 *
 * @author elieldepaula
 */
class BaseCli
{
    
    private $arrArgs = [];
    private $argsReceived = [];
    public $options = [];
    
    function __construct() {
        
        //TODO: Melhorar o encapsulamento das opçoes dos argumentos para ficar mais reutilizavel.
        
        $this->arrArgs      = ["file" => ""];
        $this->argsReceived = getopt("", ["file:"]);
        $this->options      = array_merge($this->arrArgs, $this->argsReceived);
        
        $stream = new StreamHandler(BASE_PATH . 'app/Logs/'. date('dmY') .'.log', Logger::DEBUG);
        $firephp = new FirePHPHandler();
        $this->logger = new Logger('usage_log');
        $this->logger->pushHandler($stream);
        $this->logger->pushHandler($firephp);
        
    }
    
    /**
     * Registra o log de atividades no sistema.
     * 
     * @param string $message Mensagem de log.
     * @param array $data Array de informaçoes opcionais.
     */
    public function log($message, $data = [])
    {
        $this->logger->info($message, $data);
    }
    
    // Metodos comuns aqui ...
    
}
