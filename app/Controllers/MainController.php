<?php

namespace App\Controllers;
use App\Repositories\ProductRepository as Product;

/**
 * Controller principal da aplicaçao.
 *
 * @author Eliel de Paula <dev@elieldepaula.com.br>
 */
class MainController extends BaseController
{

    public function dashboard()
    {
        
        $product        = new Product();
        $productList    = $product->list();
        
        $this->setVar('productList', $productList);
        $this->setVar('pageTitle', 'Dashboard');
        $this->render('dashboard.html');
    }

}
