<?php

namespace App\Interfaces;

/**
 * Interface para assinar os repositorios da aplicaçao, padroniza a parada toda.
 *
 * @author Eliel de Paula <dev@elieldepaula.com.br>
 */
interface CrudInterface
{
    
    /**
     * Retorna um registro.
     * @param int $id
     */
    public function find(int $id);
            
    /**
     * Lista todos os registros.
     */
    public function list();
    
    /*
     * Cria um novo registro.
     */
    public function new(object $object);
    
    /**
     * Edita um registro.
     * 
     * @param Int $id ID do registro que sera editado.
     */
    public function edit(object $object, Int $id = null);
    
    /**
     * Exclui um registro.
     * @param Int $id ID do registro que sera editado.
     */
    public function del(Int $id = null);
    
}
