<?php

/**
 * Este arquivo faz a conexao com o banco de dados usando a biblioteca de ORM.
 * 
 * @author Eliel de Paula <dev@elieldepaula.com.br>
 */

use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * Define a conexao com o banco de dados. Defina as configuraçoes de acordo
 * com o ambiende que o projeto esta sendo executado.
 */
$connection = [
    'development' => [
       'driver'    => 'mysql',
       'host'      => '127.0.0.1',
       'database'  => 'assessment-backend',
       'username'  => 'root',
       'password'  => 'tiger',
       'charset'   => 'utf8',
       'collation' => 'utf8_general_ci',
       'prefix'    => ''
    ],
    'production' => [
       'driver'    => 'mysql',
       'host'      => 'localhost',
       'database'  => '',
       'username'  => '',
       'password'  => '',
       'charset'   => 'utf8',
       'collation' => 'utf8_general_ci',
       'prefix'    => ''
    ]
];

/**
 * Faz a conexao com o banco de dados.
 */
$capsule = new Capsule;
$capsule->addConnection($connection[ENVIRONMENT]);
$capsule->setAsGlobal();
$capsule->bootEloquent();