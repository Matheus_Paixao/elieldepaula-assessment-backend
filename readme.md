# Challenge accepted!

Olá, este projeto foi desenvolvido atendendo a uma avaliação para a WebJump. Os detalhes do desafio estão [neste arquivo](challenge.md).

Resumindo, o desafio pede a criação de um sistema de cadastro de produtos e categorias, e fizeram a gentileza de adicionar alguns arquivos de html e css, já que o desafio é voltado aos back-end[ers], ainda bem ;)

Os produtos deveriam ser categorizados num esquema de N:M, e também sugeriram criar um log e upload de uma imagem para o produto. Ah, e sem framework, só com bibliotecas externas.

# Tecnologias usadas:

1. PHP 7.2
2. MySQL 5.7
3. Composer
4. GIT (Git-Flow)

### Bibliotecas usadas:

1. [nezamy/route](https://nezamy.com/route)
2. [twig/twig](https://twig.symfony.com/)
3. [illuminate/database](https://laravel.com/)
4. [phpunit/phpunit](https://phpunit.de/)
5. [monolog/monolog](http://github.com/Seldaek/monolog)

Procurei seguir ao máximo as metodologias SOLID e as PSRs, criei uma estrutura MVC bem definida e intuitiva. Adotei o [design pattern Repository](https://medium.com/@elieldepaula/padr%C3%B5es-de-design-em-php-26cd0367531b) para a camada de dados, me pareceu ser a melhor opção para não me repetir no código.

Tentei ao máximo atender aos requisitos do desafio, criando um upload para as imagens, bem como realizar os testes unitários básicos e seguir o git-flow, que é meio estranho quando estamos sozinhos num projeto, mas foi bacana.

O importador de arquivos CSV ficou bem básico, mas atende bem à demanda, ele verifica se a categoria já existe ou não antes de cadastrar, em seguida cadastra o produto. O resultado me deixou bem satisteito.

Eu me diverti muito desenvolvendo este desafio ;)

# Instruções de uso

### Requisitos

1. PHP 7.2
2. Banco de dados MySQL 5.7
3. Composer

***NOTA:** Recomendo fortemente executar tudo em ambiente Linux pois não tenho nenhuma máquina com Windows na minha vida a vários anos. Então não posso validar nenhuma dessas funcionalidades.*

### Executando

1 - Clone o projeto com o comando:
```
git clone https://elieldepaula@bitbucket.org/elieldepaula/assessment-backend.git
```
2 - Acesse a pasta do projeto:
```
cd assessment-backend
```
3 - Crie um banco de dados no MySQL, nomeie-o como preferir, sugiro "assessment-backend".

4 - Importe o arquivo assessment-backend.sql que está disponível na raíz do projeto.

5 - Configure os dados de conexão do banco de dados no arquivo "./app/Database.php":

```php
$connection = [
    'development' => [
       'driver'    => 'mysql',
       'host'      => 'localhost', // <--- Seu host.
       'database'  => 'assessment-backend', // <--- Nome do banco de dados que você criou no passo 3.
       'username'  => 'username', // <--- Nome do usuário do MySQL.
       'password'  => 'password', // <--- Senha do usuário do MySQL.
       'charset'   => 'utf8',
       'collation' => 'utf8_general_ci',
       'prefix'    => ''
    ],
    'production' => [
       'driver'    => 'mysql',
       'host'      => 'localhost',
       'database'  => '',
       'username'  => '',
       'password'  => '',
       'charset'   => 'utf8',
       'collation' => 'utf8_general_ci',
       'prefix'    => ''
    ]
];
```
6 - Instale as dependências com o composer:
```
composer install
```
7 - Execute os testes unitários a partir da pasta raíz do projeto:
```
phpunit --bootstrap vendor/autoload.php tests/WebjumpTest.php
```
***Nota:** Recomendo que instale o PHPUnit na sua máquina com o comando do linux: "``apt install phpunit``"

8 - Execute o servidor embutido do PHP
```
cd public
php -S localhost:8000
```

9 - Agora é só acessar http://localhost:8000 pelo navegador ;)

## Importador CSV

Para importar um arquivo CVS siga os passos abaixo. Note que em nosso teste os arquivos estão na pasta ``public``, se você seguiu até o passo 8, provavlemente você **não** precisará executar o passo 1.

1 - Acesse a pasta public
```
cd public
```
2 - Aplique a permissão de execução no arquivo:
```
chmod +x importer.php
```
***Nota:** Esta ação só será necessária na primeira execução.*


3 - Execute o comando:
```
./importer.php --file=import.csv
```

***Nota:** Eventualmente você poderá enfrentar problemas com o local que seu PHP está instalado no seu sistema, tradicionalmente, na maioria das distribuições Linux o PHP fica em ``/usr/bin``, caso seu PHP esteja em um local diferente, será necessário editar a primeira linha do arquivo ``importer.php`` indicando a localização correta do PHP na sua distribuição.*