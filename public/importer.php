#!/usr/bin/php -q
<?php

/**
 * Este arquivo faz a importaçao de arquivos .csv para o sistema. O arquivo deve
 * seguir a seguinte estrutura:
 * 
 * Separadores= ;
 * Cerca= [vazio]
 * Delimitador de linha= [vazio]
 * 
 * Cabeçalho:
 * nome;sku;descricao;quantidade;preco;categoria
 * 
 * A linha devera ser como:
 * Tenis Adidas;tn-123;Tenis de corrida adidas;30;259.90;Calçados|Tenis|Adulto
 * 
 * Forma de utilizaçao:
 * ./import.php --file=nome_do_arquivo.csv
 * 
 * @author Eliel de Paula <dev@elieldepaula.com.br>
 * 
 */

/**
 * Recebe os argumentos da linha de comando.
 */
$valoresPadrao = array("file" => "");
$argumentosRecebidos = getopt("", array("file:"));
$opcoes = array_merge($valoresPadrao, $argumentosRecebidos);
$arquivo = $opcoes['file'];

/**
 * Define o ambiente da aplicaçao.
 */
define('ENVIRONMENT', 'development');

date_default_timezone_set('Brazil/East');

/**
 * Define o caminho basico do projeto.
 */
define('DS', DIRECTORY_SEPARATOR, TRUE);
define('BASE_PATH', __DIR__ . DS . '..' . DS, TRUE);

/**
 * TRata a exibiçao de erros e mensagens do PHP de acordo com o ambiente.
 */
switch (ENVIRONMENT) {
    case 'development':
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        break;
    case 'production':
        ini_set('display_errors', 0);
        ini_set('display_startup_errors', 0);
        error_reporting(0);
        break;
}

/**
 * Carrega o loader do projeto.
 */
require BASE_PATH.'app/LoaderCli.php';

/**
 * Executa a parada.
 */
$importer = new \App\Controllers\ImporterController();
$importer->import();
